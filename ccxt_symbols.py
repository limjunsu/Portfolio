import ccxt

class class_coin_list:
    def __init__(self):
        self.coin_list = ['1000PEPE','1000SHIB','ADA','AGIX','ARB','ARPA','AVAX','BCH','BETH','BNB','BTC','BUSD','CFX','CHZ','DOGE','DOT','EDU'
        ,'ETC','ETH','EUR','FET','GALA','HIGH','INJ','KEY','LDO','LINA','LTC','MANA','MATIC','MNT','OGN','OP','PEPE','RNDR','SAND',
                 'SHIB','SOL','STPT','SUI','TRX','TUSD','USDC','USDP','WBTC','WLD','XLM','XRP']

coin_list = ['1000PEPE','1000SHIB','ADA','AGIX','ARB','ARPA','AVAX','BCH','BETH','BNB','BTC','BUSD','CFX','CHZ','DOGE','DOT','EDU'
    ,'ETC','ETH','EUR','FET','GALA','HIGH','INJ','KEY','LDO','LINA','LTC','MANA','MATIC','MNT','OGN','OP','PEPE','RNDR','SAND',
             'SHIB','SOL','STPT','SUI','TRX','TUSD','USDC','USDP','WBTC','WLD','XLM','XRP']


# coin_list = ['SOL']

def get_coin_list():
    return coin_list

def get_binance_spot_symbols():
    binance_mkts = ccxt.binance().fetch_markets()
    binance_all_spots = []
    for ii in binance_mkts:
        if ii['spot'] and ii['active'] == True:
            print(ii['symbol'])
            binance_all_spots.append(ii['symbol'])

    binance_spot_sym = []
    for ii in coin_list:
        if ii + '/USDT' in binance_all_spots:
            print(ii + '/USDT')
            binance_spot_sym.append(ii + '/USDT')
        if ii + '/BUSD' in binance_all_spots:
            # print(ii + '/BUSD')
            binance_spot_sym.append(ii + '/BUSD')
        if ii + '/BTC' in binance_all_spots:
            # print(ii + '/BTC')
            binance_spot_sym.append(ii + '/BTC')
        if ii + '/BNB' in binance_all_spots:
            # print(ii + '/BNB')
            binance_spot_sym.append(ii + '/BNB')
        if ii + '/TUSD' in binance_all_spots:
            # print(ii + '/TUSD')
            binance_spot_sym.append(ii + '/TUSD')
        if ii + '/ETH' in binance_all_spots:
            # print(ii + '/ETH')
            binance_spot_sym.append(ii + '/ETH')
        if ii + '/USDC' in binance_all_spots:
            # print(ii + '/USDC')
            binance_spot_sym.append(ii + '/USDC')

    return binance_spot_sym

def get_binance_futures_symbols():
    binance_mkts = ccxt.binance().fetch_markets()
    binance_all_futures = []
    for ii in binance_mkts:
        if ii['swap'] or ii['future']:
            print(ii['symbol'])
            binance_all_futures.append(ii['symbol'])

    binance_future_sym = []
    for ii in binance_all_futures:
        i = ii.split('/')[0]
        if i in coin_list:
            print(ii)
            binance_future_sym.append(ii)


    return binance_future_sym

def get_bybit_spot_symbols():
    mkts = ccxt.bybit().fetch_markets()
    all_syms = [ii['symbol'] for ii in mkts if ii['spot'] and ii['active'] == True]
    selected = [ii for ii in all_syms if ii.split('/')[0] in coin_list]
    return selected

def get_selected_spot_symbols(exchange, coins = coin_list):
    mkts = getattr(ccxt, exchange)({'newUpdates': True}).fetch_markets()
    all_syms = [ii['symbol'] for ii in mkts if ii['spot']  and ii['active'] == True]
    selected = [ii for ii in all_syms if ii.split('/')[0] in coins]
    return selected

def get_selected_future_symbols(exchange, coins = coin_list):
    mkts = getattr(ccxt, exchange)({'newUpdates': True}).fetch_markets()
    all_syms = [ ii['symbol'] for ii in mkts if (ii['swap'] or ii['future']) and ii['active'] == True]
    selected = [ii for ii in all_syms if ii.split('/')[0] in coins]
    return selected

def get_selected_type_symbols(type, exchange, coins = coin_list):
    mkts = getattr(ccxt, exchange)({'newUpdates': True}).fetch_markets()
    # all_syms = [ii['symbol'] for ii in mkts if ii[type] ]
    all_syms = [ii['symbol'] for ii in mkts if ii[type] and ii['active'] == True]
    selected = [ii for ii in all_syms if ii.split('/')[0] in coins]
    print(exchange, type, selected)
    return selected

def get_selected_option_symbols(exchange, coins = coin_list):
    mkts = getattr(ccxt, exchange)({'newUpdates': True, "options": {'defaultType': 'option'}}).fetch_markets()
    all_syms = [ii['symbol'] for ii in mkts if ii['option'] and ii['active'] == True]
    selected = [ii for ii in all_syms if ii.split('/')[0] in coins]
    return selected

# print(get_selected_type_symbols('spot', 'binance', coin_list))
# print(get_selected_type_symbols('swap', 'binance', coin_list))
# print(get_binance_spot_symbols())