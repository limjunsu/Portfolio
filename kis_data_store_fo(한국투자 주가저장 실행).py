import time, os, csv, sys
sys.path.append("/home/fengai/G/2023_Codes/modules")
from kis_fo_domestic import *


# ['AAPL', 'MSFT', 'GOOG', 'GOOGL', 'AMZN', 'NVDA',
#             'TSLA', 'META', 'AVGO', 'ASML', 'PEP', 'COST', 'ADBE', 'AZN']
# sym_list = { 'nasdaq' : ['AAPL', 'MSFT', 'GOOG', 'GOOGL', 'AMZN', 'NVDA', 'TSLA', 'META', 'AVGO', 'ASML', 'PEP', 'COST', 'ADBE', 'AZN',
#      'CSCO', 'NFLX', 'AMD', 'CMCSA', 'TMUS', 'TXN', 'INTC', 'HON', 'SNY', 'QCOM', 'INTU', 'AMAT', 'AMGN', 'ISRG',
#      'SBUX', 'MDLZ', 'BKNG', 'ADI', 'GILD', 'PDD', 'VRTX', 'ADP', 'LRCX', 'ABNB', 'REGN', 'PANW', 'PYPL', 'MU', 'EQIX',
#      'CSX', 'ATVI', 'KLAC', 'SNPS', 'CME', 'NTES', 'CDNS', 'MNST', 'WDAY', 'MELI', 'FTNT', 'ORLY', 'JD', 'MAR', 'CHTR',
#      'NXPI', 'BIDU', 'CTAS', 'DXCM', 'MCHP', 'MRNA', 'FWONK', 'LULU', 'ADSK', 'KDP', 'KHC', 'AEP', 'CPRT', 'PCAR',
#      'FWONA', 'BIIB', 'MRVL', 'EXC', 'IDXX', 'PAYX', 'ON', 'ODFL', 'ROST', 'TTD', 'SGEN', 'CSGP', 'EA', 'LI', 'GEHC'] }

#['106T12']
# ['201T10350',  '201T10352']
future, option, stock = [], [], []
nasdaq, nyse, amex = [], [], []

future = ['106T12']
option = ['201T10350']

stock = ['069500', '005930']

nasdaq = ['AAPL', 'AMZN']




sym_list = { 'future' : future, 'option' : option, 'stock' : stock,
            'nasdaq' : nasdaq,
             }


# 야간 - 프리마켓(장전거래) : 18:00 ~ 23:30 (Summer Time : 17:00 ~ 22:30), 정규장 : 23:30 ~ 06:00 (Summer Time : 22:30 ~ 05:00) |
# 주간 - 주간거래(장전거래) : 10:00 ~ 18:00 (Summer Time 10:00 ~ 17:00)    ( 애프터마켓(정규장 종료 후 거래) : 06:00 ~ 07:00 (Summer Time : 05:00 ~ 07:00) ,
# 애프터마켓 연장 신청 시(정규장 종료 후 거래) : 07:00~09:00 (Summer Time 동일), Summer Time : 3월 두번째 일요일 오전 2시 ~ 11월 첫번째 일요일 오전 2시 )

asyncio.get_event_loop().run_until_complete(connect(sym_list = sym_list, market_time= '주간' ))
asyncio.get_event_loop().close()

