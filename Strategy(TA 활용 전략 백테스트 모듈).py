import numpy as np
# from modules.backtest import *

def bt_rsi_1(df, r1, r2, initial_investment=100000):
    prices = list(df['adjClose'])
    rsis = list(df['rsi'])
    macd = list(df['macd'])

    # t0 = time.time()
    cash = initial_investment
    positions = []
    qty = 0
    p_vals = []
    buy_index = []
    sell_index = []
    sell_type = []
    buy_time = None
    sell_price = None

    for i in range(len(prices)):


        if rsis[i] < r1 and qty == 0:


            qty = int(cash / prices[i])
            cash -= prices[i] * qty
            buy_index.append(i)



        elif rsis[i] > r2 and qty > 0:
            cash += prices[i] * qty
            qty = 0
            sell_index.append(i)
            sell_price = prices[i]
            sell_type.append(0)



        positions.append(qty)
        p_vals.append(cash + qty * prices[i])


    result = cash + qty * prices[i]
    benchmark = prices[-1] / prices[0]

    mdd = calculate_mdd(p_vals)[0]

    value = result / initial_investment

    return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
            'value' : value, 'mdd' : mdd, 'benchmark' : benchmark}

def bt_rsi_2(df, r1, r2, price_rate = 1, initial_investment = 100000):   # 현재 가격이 직전 매도가 대비 얼마 이상이면 매수 안함
    prices = list(df['adjClose'])
    rsis = list(df['rsi'])

    cash = initial_investment
    positions = []
    qty = 0
    p_vals = []
    buy_index = []
    sell_index = []
    sell_type = []
    buy_time = None
    sell_price = None

    for i in range(len(prices)):
        cprice = prices[i]
        crsi = rsis[i]


        if crsi < r1 and qty == 0 and (sell_price == None or cprice >= sell_price * price_rate):

            qty = int(cash / cprice)
            cash -= cprice * qty
            buy_index.append(i)
            buy_price = cprice
            buy_time = i


        elif crsi > r2 and qty > 0:
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append(0)


        positions.append(qty)
        p_vals.append(cash + qty * cprice)


    result = cash + qty * cprice
    benchmark = prices[-1] / prices[0]

    mdd = calculate_mdd(p_vals)[0]

    value = result / initial_investment


    # return p_vals, positions, buy_index, np.array(sell_index) ,np.array(buy_prices), sell_prices, sell_type
    return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
            'value' : value, 'mdd' : mdd, 'benchmark' : benchmark}
    # return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
    #             'value' : value,  'benchmark' : benchmark}

def bt_rsi_2_1(df, r1, r1_1,  r2, price_rate = 1, initial_investment=100000):   # 현재가, 직전 매수가 비교  or RSI가 아주 낮을 때 매수
    prices = list(df['adjClose'])
    rsis = list(df['rsi'])

    cash = initial_investment
    positions = []
    qty = 0
    p_vals = []
    buy_index = []
    sell_index = []
    sell_type = []
    buy_time = None
    sell_price = None

    for i in range(len(prices)):
        cprice = prices[i]
        crsi = rsis[i]


        if qty == 0 and (  (crsi < r1 and  (sell_price == None or cprice >= sell_price * price_rate) )  or crsi < r1_1):

            qty = int(cash / cprice)
            cash -= cprice * qty
            buy_index.append(i)
            buy_price = cprice
            buy_time = i


        elif crsi > r2 and qty > 0:
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append(0)


        positions.append(qty)
        p_vals.append(cash + qty * cprice)


    result = cash + qty * cprice
    benchmark = prices[-1] / prices[0]

    mdd = calculate_mdd(p_vals)[0]

    value = result / initial_investment


    # return p_vals, positions, buy_index, np.array(sell_index) ,np.array(buy_prices), sell_prices, sell_type
    return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
            'value' : value, 'mdd' : mdd, 'benchmark' : benchmark}
    # return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
    #             'value' : value,  'benchmark' : benchmark}

def bt_rsi_3(df, r1, r2, price_rate = 1, time_cal = 3, initial_investment=100000):   # 현재가, 직전 매수가 비교 - 시간 고려 or RSI가 아주 낮을 때 매수
    prices = list(df['adjClose'])
    rsis = list(df['rsi'])

    cash = initial_investment
    positions = []
    qty = 0
    p_vals = []
    buy_index = []
    sell_index = []
    sell_type = []
    buy_time = None
    sell_price = None

    for i in range(len(prices)):
        cprice = prices[i]
        crsi = rsis[i]


        if crsi < r1 and qty == 0 and (sell_price == None or (  cprice >= sell_price * price_rate or (i - buy_time >= time_cal ) )  ):

            qty = int(cash / cprice)
            cash -= cprice * qty
            buy_index.append(i)
            buy_price = cprice
            buy_time = i


        elif crsi > r2 and qty > 0:
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append(0)


        positions.append(qty)
        p_vals.append(cash + qty * cprice)


    result = cash + qty * cprice
    benchmark = prices[-1] / prices[0]

    mdd = calculate_mdd(p_vals)[0]

    value = result / initial_investment


    # return p_vals, positions, buy_index, np.array(sell_index) ,np.array(buy_prices), sell_prices, sell_type
    return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
            'value' : value, 'mdd' : mdd, 'benchmark' : benchmark}
    # return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
    #             'value' : value,  'benchmark' : benchmark}

def bt_rsi_3_1(df, r1, r1_1, r2, price_rate = 1, time_cal = 3, initial_investment=100000):   # rsi_3 , rsi가 아주 낮을 때 매수
    prices = list(df['adjClose'])
    rsis = list(df['rsi'])

    cash = initial_investment
    positions = []
    qty = 0
    p_vals = []
    buy_index = []
    sell_index = []
    sell_type = []
    buy_time = None
    sell_price = None

    for i in range(len(prices)):
        cprice = prices[i]
        crsi = rsis[i]


        if qty == 0 and ( crsi < r1_1 or   (crsi < r1 and qty == 0 and (sell_price == None or (  cprice >= sell_price * price_rate or (i - buy_time >= time_cal ) )  ) ) ) :

            qty = int(cash / cprice)
            cash -= cprice * qty
            buy_index.append(i)
            buy_price = cprice
            buy_time = i


        elif crsi > r2 and qty > 0:
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append(0)


        positions.append(qty)
        p_vals.append(cash + qty * cprice)


    result = cash + qty * cprice
    benchmark = prices[-1] / prices[0]

    mdd = calculate_mdd(p_vals)[0]

    value = result / initial_investment


    # return p_vals, positions, buy_index, np.array(sell_index) ,np.array(buy_prices), sell_prices, sell_type
    return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
            'value' : value, 'mdd' : mdd, 'benchmark' : benchmark}
    # return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
    #             'value' : value,  'benchmark' : benchmark}

def bt_rsi_1_ts_1(df, r1, r2, ts_rate = 0.05, initial_investment=100000 ):   # Trailling Stop
    prices = list(df['adjClose'])
    rsis = list(df['rsi'])

    cash = initial_investment
    positions = []
    qty = 0
    p_vals = []
    buy_index = []
    sell_index = []
    sell_type = []
    buy_time = None
    sell_price = None
    peak_price = None
    buy_price = None

    for i in range(len(prices)):
        cprice = prices[i]
        crsi = rsis[i]

        peak_price = max(buy_price, cprice) if qty > 0 else None

        if crsi < r1 and qty == 0 :

            qty = int(cash / cprice)
            cash -= cprice * qty
            buy_index.append(i)
            buy_price = cprice
            buy_time = i


        elif crsi > r2 and qty > 0:
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append('rsi')

        elif qty > 0 and peak_price * (1 - ts_rate)  > cprice :
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append('ts')


        positions.append(qty)
        p_vals.append(cash + qty * cprice)


    result = cash + qty * cprice
    benchmark = prices[-1] / prices[0]

    mdd = calculate_mdd(p_vals)[0]

    value = result / initial_investment


    # return p_vals, positions, buy_index, np.array(sell_index) ,np.array(buy_prices), sell_prices, sell_type
    return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
            'value' : value, 'mdd' : mdd, 'benchmark' : benchmark}
    # return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
    #             'value' : value,  'benchmark' : benchmark}

def bt_rsi_2_ts_1(df, r1, r2, price_rate = 1, ts_rate = 0.05 , initial_investment=100000):   # Trailling Stop
    prices = list(df['adjClose'])
    rsis = list(df['rsi'])

    cash = initial_investment
    positions = []
    qty = 0
    p_vals = []
    buy_index = []
    sell_index = []
    sell_type = []
    buy_time = None
    sell_price = None
    peak_price = None
    buy_price = None

    for i in range(len(prices)):
        cprice = prices[i]
        crsi = rsis[i]

        peak_price = max(buy_price, cprice) if qty > 0 else None

        if crsi < r1 and qty == 0 and (sell_price == None or cprice >= sell_price * price_rate):

            qty = int(cash / cprice)
            cash -= cprice * qty
            buy_index.append(i)
            buy_price = cprice
            buy_time = i


        elif crsi > r2 and qty > 0:
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append('rsi')

        elif qty > 0 and peak_price * (1 - ts_rate)  > cprice :
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append('ts')


        positions.append(qty)
        p_vals.append(cash + qty * cprice)


    result = cash + qty * cprice
    benchmark = prices[-1] / prices[0]

    mdd = calculate_mdd(p_vals)[0]

    value = result / initial_investment


    # return p_vals, positions, buy_index, np.array(sell_index) ,np.array(buy_prices), sell_prices, sell_type
    return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
            'value' : value, 'mdd' : mdd, 'benchmark' : benchmark}
    # return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
    #             'value' : value,  'benchmark' : benchmark}

def bt_rsi_2_1_ts_1(df, r1, r1_1, r2, price_rate = 1, ts_rate = 0.05 , initial_investment = 100000):   # Trailling Stop
    prices = list(df['adjClose'])
    rsis = list(df['rsi'])

    cash = initial_investment
    positions = []
    qty = 0
    p_vals = []
    buy_index = []
    sell_index = []
    sell_type = []
    buy_time = None
    sell_price = None
    peak_price = None
    buy_price = None

    for i in range(len(prices)):
        cprice = prices[i]
        crsi = rsis[i]

        peak_price = max(buy_price, cprice) if qty > 0 else None

        if qty == 0 and (  (crsi < r1 and  (sell_price == None or cprice >= sell_price * price_rate) )  or crsi < r1_1):

            qty = int(cash / cprice)
            cash -= cprice * qty
            buy_index.append(i)
            buy_price = cprice
            buy_time = i


        elif crsi > r2 and qty > 0:
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append('rsi')

        elif qty > 0 and peak_price * (1 - ts_rate)  > cprice :
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append('ts')


        positions.append(qty)
        p_vals.append(cash + qty * cprice)


    result = cash + qty * cprice
    benchmark = prices[-1] / prices[0]

    mdd = calculate_mdd(p_vals)[0]

    value = result / initial_investment


    # return p_vals, positions, buy_index, np.array(sell_index) ,np.array(buy_prices), sell_prices, sell_type
    return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
            'value' : value, 'mdd' : mdd, 'benchmark' : benchmark}
    # return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
    #             'value' : value,  'benchmark' : benchmark}

def bt_rsi_3_ts_1(df, r1, r2, price_rate = 1, time_cal = 3, ts_rate = 0.05, initial_investment = 100000 ):   # 매수 : 현재개, 직전 매도가 비교 - 시간 고려, 매도 : Trailling Stop
    prices = list(df['adjClose'])
    rsis = list(df['rsi'])

    cash = initial_investment
    positions = []
    qty = 0
    p_vals = []
    buy_index = []
    sell_index = []
    sell_type = []
    buy_time = None
    sell_price = None
    peak_price = None
    buy_price = None

    for i in range(len(prices)):
        cprice = prices[i]
        crsi = rsis[i]

        peak_price = max(buy_price, cprice) if qty > 0 else None

        if crsi < r1 and qty == 0 and (sell_price == None or (  cprice >= sell_price * price_rate or (i - buy_time >= time_cal ) )  ):

            qty = int(cash / cprice)
            cash -= cprice * qty
            buy_index.append(i)
            buy_price = cprice
            buy_time = i


        elif crsi > r2 and qty > 0:
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append('rsi')

        elif qty > 0 and peak_price * (1 - ts_rate)  > cprice :
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append('ts')


        positions.append(qty)
        p_vals.append(cash + qty * cprice)


    result = cash + qty * cprice
    benchmark = prices[-1] / prices[0]

    mdd = calculate_mdd(p_vals)[0]

    value = result / initial_investment


    # return p_vals, positions, buy_index, np.array(sell_index) ,np.array(buy_prices), sell_prices, sell_type
    return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
            'value' : value, 'mdd' : mdd, 'benchmark' : benchmark}
    # return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
    #             'value' : value,  'benchmark' : benchmark}

def bt_rsi_3_1_ts_1(df, r1, r1_1, r2, price_rate = 1, time_cal = 3, ts_rate = 0.05 , initial_investment= 100000):   # 매수 : 현재개, 직전 매도가 비교 - 시간 고려or RSI가 아주 낮을 떄 ,   매도 : Trailling Stop
    prices = list(df['adjClose'])
    rsis = list(df['rsi'])

    cash = initial_investment
    positions = []
    qty = 0
    p_vals = []
    buy_index = []
    sell_index = []
    sell_type = []
    buy_time = None
    sell_price = None
    peak_price = None
    buy_price = None

    for i in range(len(prices)):
        cprice = prices[i]
        crsi = rsis[i]

        peak_price = max(buy_price, cprice) if qty > 0 else None

        if qty == 0 and (crsi < r1_1 or (crsi < r1 and qty == 0 and (
                sell_price == None or (cprice >= sell_price * price_rate or (i - buy_time >= time_cal))))):

            qty = int(cash / cprice)
            cash -= cprice * qty
            buy_index.append(i)
            buy_price = cprice
            buy_time = i


        elif crsi > r2 and qty > 0:
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append('rsi')

        elif qty > 0 and peak_price * (1 - ts_rate)  > cprice :
            cash += cprice * qty
            qty = 0
            sell_index.append(i)
            sell_price = cprice
            sell_type.append('ts')


        positions.append(qty)
        p_vals.append(cash + qty * cprice)


    result = cash + qty * cprice
    benchmark = prices[-1] / prices[0]

    mdd = calculate_mdd(p_vals)[0]

    value = result / initial_investment


    # return p_vals, positions, buy_index, np.array(sell_index) ,np.array(buy_prices), sell_prices, sell_type
    return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
            'value' : value, 'mdd' : mdd, 'benchmark' : benchmark}
    # return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
    #             'value' : value,  'benchmark' : benchmark}


def bt_macd(df, r1, r2, losscut = 0.05, profit = 0.5, timecut = 20, initial_investment = 100000):
    prices = list(df['adjClose'])
    rsis = list(df['rsi'])
    macd = list(df['macd'])
    macd_signal = list(df['macd_sig'])
    macd_oscilliator = list(df['macd_osc'])
    # t0 = time.time()
    cash = initial_investment
    positions = []
    qty = 0
    p_vals = []
    buy_index = []
    sell_index = []
    sell_type = []
    buy_time = None
    sell_price = None

    for i in range(len(prices)):


        # if rsis[i] < r1 and qty == 0:

        if rsis[i] < r1  and qty == 0 and macd[i] > 0:
        #     if sell_price and((i - buy_time < 20 and prices[i] < sell_price * 0.95 ) or i - buy_time >= 5):
                qty = int(cash / prices[i])
                cash -= prices[i] * qty
                buy_index.append(i)
                buy_price = prices[i]
                buy_time = i
     
        elif rsis[i] > r2 and qty > 0:
            cash += prices[i] * qty
            qty = 0
            sell_index.append(i)
            sell_price = prices[i]
            sell_type.append(0)


        positions.append(qty)
        p_vals.append(cash + qty * prices[i])


    result = cash + qty * prices[i]
    benchmark = prices[-1] / prices[0]

    mdd = calculate_mdd(p_vals)[0]

    value = result / initial_investment

    return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
            'value' : value, 'mdd' : mdd, 'benchmark' : benchmark}


def bt_rsi_macd(df, r1, r2, losscut = 0.05, profit = 0.5, timecut = 20, initial_investment = 100000):
    prices = list(df['adjClose'])
    rsis = list(df['rsi'])
    macd = list(df['macd'])

    # t0 = time.time()
    cash = initial_investment
    positions = []
    qty = 0
    p_vals = []
    buy_index = []
    sell_index = []
    sell_type = []
    buy_time = None
    sell_price = None

    for i in range(len(prices)):

        if rsis[i] < r1  and qty == 0 and macd[i] > 0:
                qty = int(cash / prices[i])
                cash -= prices[i] * qty
                buy_index.append(i)
                buy_price = prices[i]
                buy_time = i

        elif rsis[i] > r2 and qty > 0:
            cash += prices[i] * qty
            qty = 0
            sell_index.append(i)
            sell_price = prices[i]
            sell_type.append(0)


        positions.append(qty)
        p_vals.append(cash + qty * prices[i])


    result = cash + qty * prices[i]
    benchmark = prices[-1] / prices[0]

    mdd = calculate_mdd(p_vals)[0]

    value = result / initial_investment

    return {'vals' : np.array(p_vals), 'positions' : positions, 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'sell_type' : sell_type,
            'value' : value, 'mdd' : mdd, 'benchmark' : benchmark}

def backtest_supertrend(df, initial_investment = 100000000):

    is_uptrend = list(df['Supertrend'])
    prices = list(df['close'])
    cash = initial_investment
    commission = 0
    qty = 0
    buy_index = []
    sell_index = []
    p_vals = []
    positions = []
    trade_cnt = 0
    profit_trade_cnt = 0
    for i in range(2, len(df)):

        if qty == 0 and is_uptrend[i]:
            trade_cnt += 1
            qty = int(cash / prices[i])
            cash -= qty * prices[i]
            buy_index.append(i)
   
        elif qty > 0 and not is_uptrend[i]:
            cash += qty * prices[i] * (1 - commission)
            sell_index.append(i)
            qty = 0
            profit_trade_cnt += 1 if prices[i] > prices[buy_index[-1]] else 0

        p_vals.append(cash + qty * prices[i])
        positions.append(qty)
    cumulative_return = p_vals[-1]/initial_investment
    # print(prices[0], prices[-1])
    return {'cumulative_return' : cumulative_return,'vals' : np.array(p_vals), 'positions' : np.array(positions), 'buy_index' : np.array(buy_index), 'sell_index' : np.array(sell_index) , 'benchmark' : prices[-1]/prices[0] , 'winning_percentage' : profit_trade_cnt/trade_cnt, 'trade_cnt' : trade_cnt, 'profit_trade_cnt' : profit_trade_cnt}


def make_df_backtest(df1, period=10, multiplier=3, start = '2020-01-01', end = '2023-07-03', initial_investment = 100000000):
    from modules.TA import supertrend_df
    import pandas as pd

    df1.index = pd.to_datetime(df1.index, format='%Y%m%d') if df1.index.dtype == 'int64' else df1.index

    df = supertrend_df(df1, period, multiplier).join(df1)
    df = df.loc[start : end]

    return (re := backtest_supertrend(df, initial_investment))

