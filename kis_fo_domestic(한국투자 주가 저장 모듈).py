import websockets, time, os, json, requests, asyncio, yaml
# from Crypto.Cipher import AES
# from Crypto.Util.Padding import unpad
from base64 import b64decode

clearConsole = lambda: os.system('cls' if os.name in ('nt', 'dos') else 'clear')

key_bytes = 32

with open('kis_key.yaml', encoding='UTF-8') as f:
    _cfg = yaml.load(f, Loader=yaml.FullLoader)

key = _cfg['my_app']
secret = _cfg['my_sec']

### 함수 정의 ###

# AES256 DECODE
# def aes_cbc_base64_dec(key, iv, cipher_text):
#     """
#     :param key:  str type AES256 secret key value
#     :param iv: str type AES256 Initialize Vector
#     :param cipher_text: Base64 encoded AES256 str
#     :return: Base64-AES256 decodec str
#     """
#     cipher = AES.new(key.encode('utf-8'), AES.MODE_CBC, iv.encode('utf-8'))
#     return bytes.decode(unpad(cipher.decrypt(b64decode(cipher_text)), AES.block_size))


# 웹소켓 접속키 발급
def get_approval(key, secret):
    # url = https://openapivts.koreainvestment.com:29443' # 모의투자계좌
    url = 'https://openapi.koreainvestment.com:9443'  # 실전투자계좌
    headers = {"content-type": "application/json"}
    body = {"grant_type": "client_credentials",
            "appkey": key,
            "secretkey": secret}
    PATH = "oauth2/Approval"
    URL = f"{url}/{PATH}"
    res = requests.post(URL, headers=headers, data=json.dumps(body))
    approval_key = res.json()["approval_key"]
    return approval_key

# 국내 주식 호가
def stock_ord(data):
    # 0 : 장중
    # A : 장후예상
    # B : 장전예상
    # C : 9시이후의 예상가, VI발동
    # D : 시간외 단일가 예상

    recvvalue = data.split('^')  # 수신데이터를 split '^'

    data_list = [recvvalue[0], time.time(), recvvalue[1], recvvalue[4], recvvalue[24], recvvalue[3], recvvalue[23], recvvalue[13], recvvalue[33], recvvalue[14], recvvalue[34]]
    # print(data_list)
    print(recvvalue)

# 국내 주식 체결
def stock_tr(data_cnt, data):

    menulist = "유가증권단축종목코드|주식체결시간|주식현재가|전일대비부호|전일대비|전일대비율|가중평균주식가격|주식시가|주식최고가|주식최저가|매도호가1|매수호가1|체결거래량|누적거래량|누적거래대금|매도체결건수|매수체결건수|순매수체결건수|체결강도|총매도수량|총매수수량|체결구분|매수비율|전일거래량대비등락율|시가시간|시가대비구분|시가대비|최고가시간|고가대비구분|고가대비|최저가시간|저가대비구분|저가대비|영업일자|신장운영구분코드|거래정지여부|매도호가잔량|매수호가잔량|총매도호가잔량|총매수호가잔량|거래량회전율|전일동시간누적거래량|전일동시간누적거래량비율|시간구분코드|임의종료구분코드|정적VI발동기준가"
    menustr = menulist.split('|')
    pValue = data.split('^')
    data_list = ['tr', pValue[0], time.time(), pValue[1], pValue[2], pValue[10], pValue[11], pValue[12], pValue[21], pValue[36], pValue[37]]
    # i = 0
    # for cnt in range(data_cnt):  # 넘겨받은 체결데이터 개수만큼 print 한다
    #     print("### [%d / %d]" % (cnt + 1, data_cnt))
    #     print(data_list)

# 국내 주식 체결통보
def stocksigningnotice_domestic(data, key, iv):
    menulist = "고객ID|계좌번호|주문번호|원주문번호|매도매수구분|정정구분|주문종류|주문조건|주식단축종목코드|체결수량|체결단가|주식체결시간|거부여부|체결여부|접수여부|지점번호|주문수량|계좌명|체결종목명|신용구분|신용대출일자|체결종목명40|주문가격"
    menustr1 = menulist.split('|')

    # AES256 처리 단계
    aes_dec_str = aes_cbc_base64_dec(key, iv, data)
    pValue = aes_dec_str.split('^')

    if pValue[12] == '2':  # 체결통보
        print("#### 국내주식 체결 통보 ####")
    else:
        print("#### 국내주식 주문·정정·취소·거부 접수 통보 ####")

    i = 0
    for menu in menustr1:
        print("%s  [%s]" % (menu, pValue[i]))
        i += 1

# 국내 선물 체결
def future_tr(data_cnt, data):

    pValue = data.split('^')
    data_list = [pValue[0], time.time(), pValue[1], pValue[2], pValue[3], pValue[4], pValue[5], pValue[9], pValue[10], pValue[12], pValue[13], pValue[14], pValue[15], pValue[16], pValue[17], pValue[31], pValue[33], pValue[34], pValue[36], pValue[35], pValue[37]]
    # print(data_list)

#국내 선물 호가
def future_ord(data):
    # print(data)
    recvvalue = data.split('^')  # 수신데이터를 split '^'
    data_list = [recvvalue[0], time.time(), recvvalue[1], recvvalue[3], recvvalue[13], recvvalue[23], recvvalue[2], recvvalue[12], recvvalue[22], recvvalue[7], recvvalue[17], recvvalue[27]]


# 국내 옵션 호가
def option_ord(data):

    recvvalue = data.split('^')  # 수신데이터를 split '^'
    data_list = [recvvalue[0], time.time(), recvvalue[1], recvvalue[3], recvvalue[13], recvvalue[23], recvvalue[2], recvvalue[12], recvvalue[22], recvvalue[7], recvvalue[17], recvvalue[27], recvvalue[8], recvvalue[18], recvvalue[28]]


# 국내 옵션 체결
def option_tr(data_cnt, data):

    menulist = "옵션단축종목코드|영업시간|옵션현재가|전일대비부호|옵션전일대비|전일대비율|옵션시가|옵션최고가|옵션최저가|최종거래량|누적거래량|누적거래대금|HTS이론가|HTS미결제약정수량|미결제약정수량증감|시가시간|시가대비현재가부호|시가대비지수현재가|최고가시간|최고가대비현재가부호|최고가대비지수현재가|최저가시간|최저가대비현재가부호|최저가대비지수현재가|매수2비율|프리미엄값|내재가치값|시간가치값|델타|감마|베가|세타|로우|HTS내재변동성|괴리도|미결제약정직전수량증감|이론베이시스|역사적변동성|체결강도|괴리율|시장베이시스|옵션매도호가1|옵션매수호가1|매도호가잔량1|매수호가잔량1|매도체결건수|매수체결건수|순매수체결건수|총매도수량|총매수수량|총매도호가잔량|총매수호가잔량|전일거래량대비등락율|평균변동성|협의대량누적거래량|실시간상한가|실시간하한가|실시간가격제한구분"

    pValue = data.split('^')

    data_list = [pValue[0], time.time(), pValue[1], pValue[2], pValue[3], pValue[4], pValue[5], pValue[9], pValue[10], pValue[12], pValue[13], pValue[14], pValue[15], pValue[16], pValue[17], pValue[31], pValue[33], pValue[34], pValue[36], pValue[35], pValue[37] ]


            

# 해외주식호가 출력라이브러리
def stockhoka_overseas(data):
    recvvalue = data.split('^')
    ord_list = ['ord', recvvalue[0], recvvalue[1],time.time(),  recvvalue[3], recvvalue[4], recvvalue[18], recvvalue[20], recvvalue[12], recvvalue[14], recvvalue[11], recvvalue[13], recvvalue[17], recvvalue[19]  ]

    print('해외 ord')
    print(ord_list)

# 해외주식체결처리 출력라이브러리
def stockspurchase_overseas(data_cnt, data):
    pValue = data.split('^')
    data_list = [pValue[0], pValue[1], time.time(), pValue[2], pValue[3], pValue[4], pValue[5], pValue[9], pValue[10], pValue[12], pValue[25], pValue[26], pValue[27], pValue[28], pValue[29], pValue[30],  pValue[31], pValue[32], pValue[33], pValue[34], pValue[36], pValue[37] ]
    print(data_list)


# 해외주식체결통보 출력라이브러리
def stocksigningnotice_overseas(data, key, iv):
    menulist = "고객 ID|계좌번호|주문번호|원주문번호|매도매수구분|정정구분|주문종류2|단축종목코드|주문수량|체결단가|체결시간|거부여부|체결여부|접수여부|지점번호|체결수량|계좌명|체결종목명|해외종목구분|담보유형코드|담보대출일자"
    menustr1 = menulist.split('|')

    # AES256 처리 단계
    aes_dec_str = aes_cbc_base64_dec(key, iv, data)
    pValue = aes_dec_str.split('^')

    if pValue[12] == '2':  # 체결통보
        print("#### 해외주식 체결 통보 ####")
    else:
        print("#### 해외주식 주문·정정·취소·거부 접수 통보 ####")

    i = 0
    for menu in menustr1:
        print("%s  [%s]" % (menu, pValue[i]))
        i += 1


async def connect(sym_list, market_time = '정규'):
    g_appkey = key
    g_appsceret = secret
    g_approval_key = get_approval(g_appkey, g_appsceret)
    print("approval_key [%s]" % (g_approval_key))

    # url = 'ws://ops.koreainvestment.com:31000' # 모의투자계좌
    url = 'ws://ops.koreainvestment.com:21000'  # 실전투자계좌

    # code_list = code_list_ord + code_list_tr
    code_list_ord0, code_list_tr0, code_list_ord1, code_list_tr1, code_list_ord2, code_list_tr2, code_list_ord3, code_list_tr3 = [], [], [], [], [], [], [], []
    future_list_ord, future_list_tr, option_list_ord, option_list_tr = [], [], [], []

    if 'stock' in sym_list.keys():
        code_list_ord0 = [['1', 'H0STASP0', sym] for sym in sym_list['stock']]
        code_list_tr0 = [['1', 'H0STCNT0', sym] for sym in sym_list['stock']]

    if 'future' in sym_list.keys():
        future_list_ord = [['1', 'H0IFASP0', sym] for sym in sym_list['future']]
        future_list_tr = [['1', 'H0IFCNT0', sym] for sym in sym_list['future']]
    if 'option' in sym_list.keys():
        option_list_ord = [['1', 'H0IOASP0', sym] for sym in sym_list['option']]
        option_list_tr = [['1', 'H0IOCNT0', sym] for sym in sym_list['option']]


    if market_time == '주간':
        if 'nasdaq' in sym_list.keys():
            code_list_ord1 = [['1', 'HDFSASP0', 'RBAQ' + sym] for sym in sym_list['nasdaq']]
            code_list_tr1 = [['1', 'HDFSCNT0', 'RBAQ' + sym] for sym in sym_list['nasdaq']]
        if 'nyse' in sym_list.keys():
            code_list_ord2 = [['1', 'HDFSASP0', 'RBAY' + sym] for sym in sym_list['nyse']]
            code_list_tr2 = [['1', 'HDFSCNT0', 'RBAY' + sym] for sym in sym_list['nyse']]
        if 'amex' in sym_list.keys():
            code_list_ord3 = [['1', 'HDFSASP0', 'RBAA' + sym] for sym in sym_list['amex']]
            code_list_tr3 = [['1', 'HDFSCNT0', 'RBAA' + sym] for sym in sym_list['amex']]

    else:
        if 'nasdaq' in sym_list.keys():
            code_list_ord1 = [['1', 'HDFSASP0', 'DNAS' + sym] for sym in sym_list['nasdaq']]
            code_list_tr1 = [['1', 'HDFSCNT0', 'DNAS' + sym] for sym in sym_list['nasdaq']]
        if 'nyse' in sym_list.keys():
            code_list_ord2 = [['1', 'HDFSASP0', 'DNYS' + sym] for sym in sym_list['nyse']]
            code_list_tr2 = [['1', 'HDFSCNT0', 'DNYS' + sym] for sym in sym_list['nyse']]
        if 'amex' in sym_list.keys():
            code_list_ord3 = [['1', 'HDFSASP0', 'DAMS' + sym] for sym in sym_list['amex']]
            code_list_tr3 = [['1', 'HDFSCNT0', 'DAMS' + sym] for sym in sym_list['amex']]

    code_list = code_list_ord0 + code_list_tr0 + code_list_ord1 + code_list_tr1 + code_list_ord2 + code_list_tr2 + code_list_ord3 + code_list_tr3 + future_list_ord + future_list_tr + option_list_ord + option_list_tr

    print('code_list', code_list)
    senddata_list = []

    for i, j, k in code_list:
        temp = '{"header":{"approval_key": "%s","custtype":"P","tr_type":"%s","content-type":"utf-8"},"body":{"input":{"tr_id":"%s","tr_key":"%s"}}}' % (
        g_approval_key, i, j, k)
        senddata_list.append(temp)

    async with websockets.connect(url, ping_interval=30) as websocket:

        for senddata in senddata_list:
            await websocket.send(senddata)
            await asyncio.sleep(0.1)
            print(f"Input Command is :{senddata}")

        while True:

            try:

                data = await websocket.recv()
                await asyncio.sleep(0.0001)



                if data[0] == '0':
                    recvstr = data.split('|')  # 수신데이터가 실데이터 이전은 '|'로 나뉘어져있어 split
                    trid0 = recvstr[1]

                    if trid0 == "H0STASP0":  # 국내 주식 호가
                        stock_ord(recvstr[3])
                        # await asyncio.sleep(0.1)

                    elif trid0 == "H0STCNT0":  # 국내 주식 체결
                        data_cnt = int(recvstr[2])
                        stock_tr(data_cnt, recvstr[3])
                        # await asyncio.sleep(0.1)

                    if trid0 == "HDFSASP0":  # 실시간 주식 호가(미국)
                        stockhoka_overseas(recvstr[3])
                        # await asyncio.sleep(0.1)

                    elif trid0 == "HDFSCNT0":  # 해외주식체결
                        data_cnt = int(recvstr[2])
                        stockspurchase_overseas(data_cnt, recvstr[3])
                        # await asyncio.sleep(0.1)

                    elif trid0 == "H0IFASP0":  # 국내 선물 호가
                        future_ord(recvstr[3])

                    elif trid0 == "H0IFCNT0":  # 국내 선물 체결
                        data_cnt = int(recvstr[2])  # 체결데이터 개수
                        future_tr(data_cnt, recvstr[3])

                    elif trid0 == "H0IOASP0":   # 국내 지수옵션 호가
                        option_ord(recvstr[3])

                    elif trid0 == "H0IOCNT0":   # 국내 지수옵션 체결
                        data_cnt = int(recvstr[2])
                        option_tr(data_cnt, recvstr[3])



                elif data[0] == '1':
                    recvstr = data.split('|')  # 수신데이터가 실데이터 이전은 '|'로 나뉘어져있어 split
                    trid0 = recvstr[1]

                    if trid0 == "K0STCNI0" or trid0 == "K0STCNI9":  # 주실체결 통보 처리
                        stocksigningnotice_domestic(recvstr[3], aes_key, aes_iv)

                    elif trid0 == "H0GSCNI0" or trid0 == "H0GSCNI9" :  # 해외주식 실시간체결통보[실시간-009]
                        stocksigningnotice_overseas(recvstr[3], aes_key, aes_iv)

                else:

                    jsonObject = json.loads(data)
                    trid = jsonObject["header"]["tr_id"]

                    if trid != "PINGPONG":
                        rt_cd = jsonObject["body"]["rt_cd"]

                        if rt_cd == '1':  # 에러일 경우 처리
                            if jsonObject["body"]["msg1"] != 'ALREADY IN SUBSCRIBE':
                                print("### ERROR RETURN CODE [ %s ][ %s ] MSG [ %s ]" % (
                                jsonObject["header"]["tr_key"], rt_cd, jsonObject["body"]["msg1"]))
                            break

                        elif rt_cd == '0':  # 정상일 경우 처리
                            # print("### RETURN CODE [ %s ][ %s ] MSG [ %s ]" % (
                            # jsonObject["header"]["tr_key"], rt_cd, jsonObject["body"]["msg1"]))

                            # 체결통보 처리를 위한 AES256 KEY, IV 처리 단계
                            if trid == "K0STCNI0" or trid == "K0STCNI9" or trid == "H0STCNI0" or trid == "H0STCNI9":
                                aes_key = jsonObject["body"]["output"]["key"]
                                aes_iv = jsonObject["body"]["output"]["iv"]
                                print("### TRID [%s] KEY[%s] IV[%s]" % (trid, aes_key, aes_iv))

                            elif trid == "H0GSCNI0" or trid == "H0GSCNI9" or trid == "H0GSCNI0" or trid == "H0GSCNI9":
                                aes_key = jsonObject["body"]["output"]["key"]
                                aes_iv = jsonObject["body"]["output"]["iv"]
                                print("### TRID [%s] KEY[%s] IV[%s]" % (trid, aes_key, aes_iv))

                    elif trid == "PINGPONG":
                        print("### RECV [PINGPONG] [%s]" % (data))
                        await websocket.pong(data)
                        print("### SEND [PINGPONG] [%s]" % (data))

            except websockets.ConnectionClosed:
                continue


# 비동기로 서버에 접속한다.
# asyncio.get_event_loop().run_until_complete(connect())
# asyncio.get_event_loop().close()
