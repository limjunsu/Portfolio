OPENAI_API_KEY = 'sk-'
import os, time, csv
import openai
import pandas as pd
from langchain import OpenAI, ConversationChain, LLMChain, PromptTemplate, LLMMathChain
from langchain.memory import ConversationBufferWindowMemory, ConversationKGMemory, ConversationBufferMemory

from langchain.agents import create_csv_agent
from langchain.agents.agent_types import AgentType
from langchain.agents import initialize_agent, Tool

from langchain.chat_models import ChatOpenAI
from langchain.chains import LLMChain, SequentialChain
from langchain.prompts.chat import ChatPromptTemplate
from langchain.text_splitter import CharacterTextSplitter
from langchain.prompts.chat import ChatPromptTemplate
os.environ['OPENAI_API_KEY'] = OPENAI_API_KEY
writer_llm = ChatOpenAI(temperature=0, model = 'gpt-3.5-turbo-0613')
df = pd.read_csv('files/20230717_2192.csv', index_col=0)

df[df['SN'] == 0]['text'].values

def read_prompt_template(file_path: str) -> str:
    with open(file_path, "r") as f:
        prompt_template = f.read()

    return prompt_template

def intent(sn, template = "다음을 3 문장 이내로 요약하라.  {talk}"):
    writer_prompt_template = ChatPromptTemplate.from_template(
        template=template
    )
    writer_chain = LLMChain(llm=writer_llm, prompt=writer_prompt_template, verbose=False)
    # print(result := writer_chain.predict(talk=df[df['SN'] == sn]['text'].values))
    result = writer_chain.predict(talk=df[df['SN'] == sn]['text'].values)
    return result




intent_list = []
for ii in range(len(df)):
    print(ii)
    intent_list.append(intent(ii, '''
다음 <민원상담>에서 민원인이 상담 전화를 건 이유를 <예제>처럼 한 문장으로 요약하라. 어법 및 맞춤법을 교정하라  <민원상담> {talk} </민원상담>.

<예제>
'수주팔봉 이용 문의'
'폐보도블럭 받을수 있는지 문의'
'불법주차 신고'
'결격사유 조회'
'차적조회 요청'
</예제>

                '''))
df1 = pd.DataFrame(intent_list)
# df1.to_csv('files/intent_list.csv', encoding='utf-8-sig')
print(df1)