import ccxt.pro
import asyncio
import time, os, csv, sys
from datetime import datetime as dtdt
# sys.path.append("/home/fengai/G/2023_codes/modules")
from ccxt_symbols import *


class DataCollector:
    def __init__(self):
        # self.path = '/home/fengai/Data/ccxt/exchanges'
        # self.path = '/home/fengai/O/Data/crypto'

        self.date = dtdt.now().date()

    def store_data(self, data, exchange, s1, s2, market_type):
        csv_name = f"{self.date}_{s1}-{s2}_{market_type}_{exchange}.csv"
        # csv_name = f"{s1}-{s2}.csv"
        # csv_ = open(os.path.join(self.path, csv_name), 'a', encoding='utf-8-sig', newline='')
        csv_ = open(os.path.join(self.path, f'{exchange}/' + csv_name), 'a', encoding='utf-8-sig', newline='')
        writer = csv.writer(csv_)  # csv 저장
        writer.writerow(data)
        csv_.close()


    async def get_trade(self, exchange, symbol, market_type = 'spot'):
        s1, s2 = symbol.split('/')

        while True:
            try:
                trade = await exchange.watch_trades(symbol)
                now = exchange.milliseconds()
                dic_ = trade[0]
                tr_list = [time.time(), dic_['datetime'], now, symbol, market_type,
                           dic_['price'], dic_['amount'], dic_['side']
                           ]
                # print(tr_list)
                self.store_data(data=tr_list, exchange=exchange, s1=s1, s2=s2, market_type=market_type)


            except Exception as e:
                print(exchange, symbol, 'trade error', market_type, str(e))
                # break
                pass

    async def get_orderbook(self, exchange, symbol, market_type = 'spot'):
        s1, s2 = symbol.split('/')

        while True:
            try:
                ticker = await exchange.watch_order_book(symbol)
                now = exchange.milliseconds()
                ord_list = [time.time(), ticker['datetime'], now, symbol, market_type,
                            ticker['bids'][0][0], ticker['bids'][0][1], ticker['bids'][1][0], ticker['bids'][1][1],
                            ticker['asks'][0][0], ticker['asks'][0][1], ticker['asks'][1][0], ticker['asks'][1][1]
                            ] if ticker['datetime'] else [time.time(), None, now, symbol, market_type,
                            ticker['bids'][0][0], ticker['bids'][0][1], ticker['bids'][1][0], ticker['bids'][1][1],
                            ticker['asks'][0][0], ticker['asks'][0][1], ticker['asks'][1][0], ticker['asks'][1][1]
                            ]
                self.store_data(data=ord_list, exchange=exchange, s1=s1, s2=s2, market_type=market_type )
                # print(exchange, symbol, market_type)

            except Exception as e:
                print(exchange, symbol, 'orderbook error', market_type, str(e))
                # print(ticker)
                # break
                pass

    async def exchange_loop(self, exchange_id, symbols, market_type = 'spot', stream_type = 'all' ):

        if '/' in exchange_id:
            exchange_id, market_type = exchange_id.split('/')[0], exchange_id.split('/')[1]
        else:
            exchange_id = exchange_id
            # market_type = market_type


        if exchange_id == 'binance':
            if market_type == 'future' or market_type == 'swap':
                exchange = getattr(ccxt.pro, exchange_id)({
                    'newUpdates': True,'options': {'defaultType': 'future'}})
            elif market_type == 'spot' :
                exchange = getattr(ccxt.pro, exchange_id)({
                    'newUpdates': True,'options': {'defaultType': 'spot'}})

            trade_tasks = [self.get_trade(exchange, symbol, market_type) for symbol in symbols]

            orderbook_tasks = [self.get_orderbook(exchange, symbol, market_type) for symbol in symbols]

            if stream_type == 'trade':
                tasks = trade_tasks
            elif stream_type == 'orderbook':
                tasks = orderbook_tasks
            else:
                tasks =  trade_tasks + orderbook_tasks
            await asyncio.gather(*tasks)
            await exchange.close()

        else:
            exchange = getattr(ccxt.pro, exchange_id)({'newUpdates': True})
            orderbook_tasks = [self.get_orderbook(exchange, symbol, market_type) for symbol in symbols]
            trade_tasks = [self.get_trade(exchange, symbol, market_type) for symbol in symbols]

            # tasks =  trade_tasks
            if stream_type == 'trade':
                tasks = trade_tasks
            elif stream_type == 'orderbook':
                tasks = orderbook_tasks
            else:
                tasks = trade_tasks + orderbook_tasks
            await asyncio.gather(*tasks)
            await exchange.close()

    async def check_date(self):
        while True:
            self.dt = dtdt.now()
            self.date = self.dt.date()

            await asyncio.sleep(10)

    async def main(self, exchange, market_type, stream_type):

        exchanges = {
            f'f{exchange}/{market_type}' : get_selected_type_symbols(market_type, exchange)
        }
        exchange_tasks =   [self.exchange_loop(exchange, symbols, market_type = market_type, stream_type=stream_type) for exchange_id, symbols in exchanges.items()]
        await asyncio.gather(*exchange_tasks)


async def run_mains():
    collector = DataCollector()
    await asyncio.gather(collector.check_date(), collector.main('binance', 'spot', 'all'), collector.main('bybit', 'spot', 'all'), collector.main('upbit', 'spot', 'trade'), collector.main('upbit', 'spot', 'orderbook')) # 실행 가능


error_cnt = 0
path_data = path_data = '/home/fengai/Data'
try:
    asyncio.run(run_mains())
except Exception as e:
    error_cnt += 1
    print(err_t := [dtdt.now(), 'cnt', error_cnt])
    print(error_cnt, '번째 에러 발생 : ', [e])
    print('')
    csv_name = '_data_store_error_log.csv'
    csv_ = open(os.path.join(path_data, csv_name), 'a', encoding='utf-8-sig', newline='')
    writer = csv.writer(csv_)  # csv 저장
    writer.writerow(err_t)
    writer.writerow([e])
    csv_.close()
    asyncio.run(run_mains())
