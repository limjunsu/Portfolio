OPENAI_API_KEY = 'sk-'
import os, time, csv, cx_Oracle
import openai
import pandas as pd
from langchain import OpenAI, ConversationChain, LLMChain, PromptTemplate, LLMMathChain
from langchain.memory import ConversationBufferWindowMemory, ConversationKGMemory, ConversationBufferMemory

from langchain.agents import create_csv_agent
from langchain.agents.agent_types import AgentType
from langchain.agents import initialize_agent, Tool

from langchain.chat_models import ChatOpenAI
from langchain.chains import LLMChain, SequentialChain
from langchain.prompts.chat import ChatPromptTemplate
from langchain.text_splitter import CharacterTextSplitter
from langchain.prompts.chat import ChatPromptTemplate
os.environ['OPENAI_API_KEY'] = OPENAI_API_KEY
openai.api_key = OPENAI_API_KEY

df = pd.read_csv('files/20230717_2192.csv', index_col=0)
writer_llm = ChatOpenAI(temperature=0, model = 'gpt-3.5-turbo-0613')

def read_prompt_template(file_path: str) -> str:
    with open(file_path, "r") as f:
        prompt_template = f.read()

    return prompt_template



def summarize_openai_5(sn, template, token=512, temperature = 0, top_p = 1, freq = 0, pres = 0, model = "gpt-3.5-turbo-0613"):
  response = openai.ChatCompletion.create(
    model = model,
    messages=[
      {
        "role": "system",
        "content": read_prompt_template(f'templates/{template}.txt')
      },
      {
        "role": "user",
        "content":  '통화 :' + str(df[df['SN'] == sn]['text'].values )
      },
      {
        "role": "assistant",
        "content": "요약 : "
      }
    ],
    temperature=temperature,
    max_tokens=token,
    top_p=top_p,
    frequency_penalty=freq,
    presence_penalty=pres
  )
  # print(response.choices[0].message["content"])
  return response.choices[0].message["content"]


sum_list = []
for ii in range(len(df)):
  print(ii)
  sum_list.append(aa := summarize_openai_5(ii, 'sys10'))
  print(aa)


df_sum = pd.DataFrame(sum_list)
df_sum.index = range(1, 88)
df_sum.to_csv('files/summary.csv', encoding= 'utf-8-sig')