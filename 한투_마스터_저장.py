import pandas as pd
import urllib.request
import ssl
import zipfile
import os

base_dir = r'/home/fengai/G/2023_codes/주가 저장/한투'
os.chdir(base_dir)
# path = '/home/fengai/G/2023_Codes/주가 저장

# 국내 주가지수 선물옵션
def get_domestic_future_master_dataframe():
    ssl._create_default_https_context = ssl._create_unverified_context
    urllib.request.urlretrieve("https://new.real.download.dws.co.kr/common/master/fo_idx_code_mts.mst.zip", "fo_idx_code_mts.mst.zip")
    fo_idx_code_zip = zipfile.ZipFile('fo_idx_code_mts.mst.zip')
    fo_idx_code_zip.extractall()
    fo_idx_code_zip.close()
    file_name = "fo_idx_code_mts.mst"
    columns = ['상품종류', '단축코드', '표준코드', ' 한글종목명', ' ATM구분',
               ' 행사가', ' 월물구분코드', ' 기초자산 단축코드', ' 기초자산 명']
    df = pd.read_table(file_name, sep='|', encoding='cp949', header=None)
    df.columns = columns
    os.remove('fo_idx_code_mts.mst')
    os.remove('fo_idx_code_mts.mst.zip')
    df.to_excel('한투_주가지수_선물옵션_마스터.xlsx', index=False)  # 현재 위치에 엑셀파일로 저장
    return df

# 국내 상품채권외환 선물옵션
def get_domestic_com_future_master_dataframe():
    ssl._create_default_https_context = ssl._create_unverified_context
    urllib.request.urlretrieve("https://new.real.download.dws.co.kr/common/master/fo_com_code.mst.zip",
                               "fo_com_code.mst.zip")
    fo_com_code_zip = zipfile.ZipFile('fo_com_code.mst.zip')
    fo_com_code_zip.extractall()
    fo_com_code_zip.close()
    file_name =  "fo_com_code.mst"
    # df1 : '상품구분','상품종류','단축코드','표준코드','한글종목명'
    tmp_fil1 =  "fo_com_code_part1.tmp"
    tmp_fil2 =  "fo_com_code_part2.tmp"
    wf1 = open(tmp_fil1, mode="w")
    wf2 = open(tmp_fil2, mode="w")
    with open(file_name, mode="r", encoding="cp949") as f:
        for row in f:
            rf1 = row[0:55]
            rf1_1 = rf1[0:1]
            rf1_2 = rf1[1:2]
            rf1_3 = rf1[2:11].strip()
            rf1_4 = rf1[11:23].strip()
            rf1_5 = rf1[23:55].strip()
            wf1.write(rf1_1 + ',' + rf1_2 + ',' + rf1_3 + ',' + rf1_4 + ',' + rf1_5 + '\n')
            rf2 = row[55:].lstrip()
            wf2.write(rf2)
    wf1.close()
    wf2.close()
    part1_columns = ['상품구분', '상품종류', '단축코드', '표준코드', '한글종목명']
    df1 = pd.read_csv(tmp_fil1, header=None, names=part1_columns, encoding='utf-8')
    # df2 : '월물구분코드','기초자산 단축코드','기초자산 명'
    tmp_fil3 =  "fo_com_code_part3.tmp"
    wf3 = open(tmp_fil3, mode="w")
    with open(tmp_fil2, mode="r", encoding="utf-8") as f:
        for row in f:
            rf2 = row[:]
            rf2_1 = rf2[8:9]
            rf2_2 = rf2[9:12]
            rf2_3 = rf2[12:].strip()
            wf3.write(rf2_1 + ',' + rf2_2 + ',' + rf2_3 + '\n')
    wf3.close()
    part2_columns = ['월물구분코드', '기초자산 단축코드', '기초자산 명']
    df2 = pd.read_csv(tmp_fil3, header=None, names=part2_columns, encoding='utf-8')
    DF = pd.concat([df1, df2], axis=1)
    os.remove(tmp_fil1)
    os.remove(tmp_fil2)
    os.remove(tmp_fil3)
    os.remove('fo_com_code.mst')
    os.remove('fo_com_code.mst.zip')
    DF.to_excel('한투_상품채권외환_선물옵션_마스터.xlsx', index=False)  # 현재 위치에 엑셀파일로 저장
    return DF # 국내

# 국내 주식 선물옵션
def get_domestic_stk_future_master_dataframe():
    ssl._create_default_https_context = ssl._create_unverified_context
    urllib.request.urlretrieve("https://new.real.download.dws.co.kr/common/master/fo_stk_code_mts.mst.zip","fo_stk_code_mts.mst.zip")
    fo_stk_code_zip = zipfile.ZipFile('fo_stk_code_mts.mst.zip')
    fo_stk_code_zip.extractall()
    fo_stk_code_zip.close()

    file_name =  "fo_stk_code_mts.mst"
    columns = ['상품종류', '단축코드', '표준코드', ' 한글종목명', ' ATM구분',
               ' 행사가', ' 월물구분코드', ' 기초자산 단축코드', ' 기초자산 명']
    df = pd.read_table(file_name, sep='|', encoding='cp949', header=None)
    df.columns = columns
    os.remove('fo_stk_code_mts.mst')
    os.remove('fo_stk_code_mts.mst.zip')
    df.to_excel('한투_주식_선물옵션_마스터.xlsx', index=False)  # 현재 위치에 엑셀파일로 저장
    return df

# 국내 코스닥 주식 마스터
def get_kosdaq_master_dataframe():
    ssl._create_default_https_context = ssl._create_unverified_context
    urllib.request.urlretrieve("https://new.real.download.dws.co.kr/common/master/kosdaq_code.mst.zip",  "kosdaq_code.zip")
    kosdaq_zip = zipfile.ZipFile('kosdaq_code.zip')
    kosdaq_zip.extractall()
    kosdaq_zip.close()
    if os.path.exists("kosdaq_code.zip"):
        os.remove("kosdaq_code.zip")
    file_name = "kosdaq_code.mst"
    tmp_fil1 = "kosdaq_code_part1.tmp"
    tmp_fil2 ="kosdaq_code_part2.tmp"
    wf1 = open(tmp_fil1, mode="w")
    wf2 = open(tmp_fil2, mode="w")
    with open(file_name, mode="r", encoding="cp949") as f:
        for row in f:
            rf1 = row[0:len(row) - 222]
            rf1_1 = rf1[0:9].rstrip()
            rf1_2 = rf1[9:21].rstrip()
            rf1_3 = rf1[21:].strip()
            wf1.write(rf1_1 + ',' + rf1_2 + ',' + rf1_3 + '\n')
            rf2 = row[-222:]
            wf2.write(rf2)
    wf1.close()
    wf2.close()
    part1_columns = ['단축코드', '표준코드', '한글종목명']
    df1 = pd.read_csv(tmp_fil1, header=None, names=part1_columns, encoding='utf-8')
    field_specs = [2, 1,
                   4, 4, 4, 1, 1,
                   1, 1, 1, 1, 1,
                   1, 1, 1, 1, 1,
                   1, 1, 1, 1, 1,
                   1, 1, 1, 1, 9,
                   5, 5, 1, 1, 1,
                   2, 1, 1, 1, 2,
                   2, 2, 3, 1, 3,
                   12, 12, 8, 15, 21,
                   2, 7, 1, 1, 1,
                   1, 9, 9, 9, 5,
                   9, 8, 9, 3, 1,
                   1, 1
                   ]
    part2_columns = ['증권그룹구분코드', '시가총액 규모 구분 코드 유가',
                     '지수업종 대분류 코드', '지수 업종 중분류 코드', '지수업종 소분류 코드', '벤처기업 여부 (Y/N)',
                     '저유동성종목 여부', 'KRX 종목 여부', 'ETP 상품구분코드', 'KRX100 종목 여부 (Y/N)',
                     'KRX 자동차 여부', 'KRX 반도체 여부', 'KRX 바이오 여부', 'KRX 은행 여부', '기업인수목적회사여부',
                     'KRX 에너지 화학 여부', 'KRX 철강 여부', '단기과열종목구분코드', 'KRX 미디어 통신 여부',
                     'KRX 건설 여부', '(코스닥)투자주의환기종목여부', 'KRX 증권 구분', 'KRX 선박 구분',
                     'KRX섹터지수 보험여부', 'KRX섹터지수 운송여부', 'KOSDAQ150지수여부 (Y,N)', '주식 기준가',
                     '정규 시장 매매 수량 단위', '시간외 시장 매매 수량 단위', '거래정지 여부', '정리매매 여부',
                     '관리 종목 여부', '시장 경고 구분 코드', '시장 경고위험 예고 여부', '불성실 공시 여부',
                     '우회 상장 여부', '락구분 코드', '액면가 변경 구분 코드', '증자 구분 코드', '증거금 비율',
                     '신용주문 가능 여부', '신용기간', '전일 거래량', '주식 액면가', '주식 상장 일자', '상장 주수(천)',
                     '자본금', '결산 월', '공모 가격', '우선주 구분 코드', '공매도과열종목여부', '이상급등종목여부',
                     'KRX300 종목 여부 (Y/N)', '매출액', '영업이익', '경상이익', '단기순이익', 'ROE(자기자본이익률)',
                     '기준년월', '전일기준 시가총액 (억)', '그룹사 코드', '회사신용한도초과여부', '담보대출가능여부', '대주가능여부'
                     ]
    df2 = pd.read_fwf(tmp_fil2, widths=field_specs, names=part2_columns)
    df = pd.merge(df1, df2, how='outer', left_index=True, right_index=True)
    del (df1)
    del (df2)
    os.remove(tmp_fil1)
    os.remove(tmp_fil2)
    os.remove('kosdaq_code.mst')
    df.to_excel('한투 코스닥 주식 마스터.xlsx', index=False)
    return df

# 국내 코스피 주식 마스터
def get_kospi_master_dataframe():
    ssl._create_default_https_context = ssl._create_unverified_context
    urllib.request.urlretrieve("https://new.real.download.dws.co.kr/common/master/kospi_code.mst.zip", "kospi_code.zip")
    kospi_zip = zipfile.ZipFile('kospi_code.zip')
    kospi_zip.extractall()
    kospi_zip.close()
    if os.path.exists("kospi_code.zip"):
        os.remove("kospi_code.zip")
    file_name = "kospi_code.mst"
    tmp_fil1 =  "kospi_code_part1.tmp"
    tmp_fil2 =  "kospi_code_part2.tmp"
    wf1 = open(tmp_fil1, mode="w")
    wf2 = open(tmp_fil2, mode="w")
    with open(file_name, mode="r", encoding="cp949") as f:
        for row in f:
            rf1 = row[0:len(row) - 228]
            rf1_1 = rf1[0:9].rstrip()
            rf1_2 = rf1[9:21].rstrip()
            rf1_3 = rf1[21:].strip()
            wf1.write(rf1_1 + ',' + rf1_2 + ',' + rf1_3 + '\n')
            rf2 = row[-228:]
            wf2.write(rf2)
    wf1.close()
    wf2.close()
    part1_columns = ['단축코드', '표준코드', '한글명']
    df1 = pd.read_csv(tmp_fil1, header=None, names=part1_columns, encoding='utf-8')
    field_specs = [2, 1, 4, 4, 4,
                   1, 1, 1, 1, 1,
                   1, 1, 1, 1, 1,
                   1, 1, 1, 1, 1,
                   1, 1, 1, 1, 1,
                   1, 1, 1, 1, 1,
                   1, 9, 5, 5, 1,
                   1, 1, 2, 1, 1,
                   1, 2, 2, 2, 3,
                   1, 3, 12, 12, 8,
                   15, 21, 2, 7, 1,
                   1, 1, 1, 1, 9,
                   9, 9, 5, 9, 8,
                   9, 3, 1, 1, 1
                   ]
    part2_columns = ['그룹코드', '시가총액규모', '지수업종대분류', '지수업종중분류', '지수업종소분류',
                     '제조업', '저유동성', '지배구조지수종목', 'KOSPI200섹터업종', 'KOSPI100',
                     'KOSPI50', 'KRX', 'ETP', 'ELW발행', 'KRX100',
                     'KRX자동차', 'KRX반도체', 'KRX바이오', 'KRX은행', 'SPAC',
                     'KRX에너지화학', 'KRX철강', '단기과열', 'KRX미디어통신', 'KRX건설',
                     'Non1', 'KRX증권', 'KRX선박', 'KRX섹터_보험', 'KRX섹터_운송',
                     'SRI', '기준가', '매매수량단위', '시간외수량단위', '거래정지',
                     '정리매매', '관리종목', '시장경고', '경고예고', '불성실공시',
                     '우회상장', '락구분', '액면변경', '증자구분', '증거금비율',
                     '신용가능', '신용기간', '전일거래량', '액면가', '상장일자',
                     '상장주수', '자본금', '결산월', '공모가', '우선주',
                     '공매도과열', '이상급등', 'KRX300', 'KOSPI', '매출액',
                     '영업이익', '경상이익', '당기순이익', 'ROE', '기준년월',
                     '시가총액', '그룹사코드', '회사신용한도초과', '담보대출가능', '대주가능'
                     ]
    df2 = pd.read_fwf(tmp_fil2, widths=field_specs, names=part2_columns)
    df = pd.merge(df1, df2, how='outer', left_index=True, right_index=True)
    # clean temporary file and dataframe
    del (df1)
    del (df2)
    os.remove(tmp_fil1)
    os.remove(tmp_fil2)
    os.remove('kospi_code.mst')
    df.to_excel('한투 코스피 주식 마스터.xlsx', index=False)
    return df

# 해외 선물옵션 마스터
def get_overseas_future_master_dataframe():
    ssl._create_default_https_context = ssl._create_unverified_context
    urllib.request.urlretrieve("https://new.real.download.dws.co.kr/common/master/ffcode.mst.zip",
                                "ffcode.mst.zip")


    nas_zip = zipfile.ZipFile('ffcode.mst.zip')
    nas_zip.extractall()
    nas_zip.close()

    file_name = "ffcode.mst"
    columns = ['종목코드', '서버자동주문 가능 종목 여부', '서버자동주문 TWAP 가능 종목 여부', '서버자동 경제지표 주문 가능 종목 여부',
               '필러', '종목한글명', '거래소코드 (ISAM KEY 1)', '품목코드 (ISAM KEY 2)', '품목종류', '출력 소수점', '계산 소수점',
               '틱사이즈', '틱가치', '계약크기', '가격표시진법', '환산승수', '최다월물여부 0:원월물 1:최다월물',
               '최근월물여부 0:원월물 1:최근월물', '스프레드여부', '스프레드기준종목 LEG1 여부', '서브 거래소 코드']
    df = pd.DataFrame(columns=columns)
    ridx = 1

    with open(file_name, mode="r", encoding="cp949") as f:
        for row in f:
            a = row[:32]  # 종목코드
            b = row[32:33].rstrip()  # 서버자동주문 가능 종목 여부
            c = row[33:34].rstrip()  # 서버자동주문 TWAP 가능 종목 여부
            d = row[34:35]  # 서버자동 경제지표 주문 가능 종목 여부
            e = row[35:82].rstrip()  # 필러
            f = row[82:107].rstrip()  # 종목한글명
            g = row[-92:-82]  # 거래소코드 (ISAM KEY 1)
            h = row[-82:-72].rstrip()  # 품목코드 (ISAM KEY 2)
            i = row[-72:-69].rstrip()  # 품목종류
            j = row[-69:-64]  # 출력 소수점
            k = row[-64:-59].rstrip()  # 계산 소수점
            l = row[-59:-45].rstrip()  # 틱사이즈
            m = row[-45:-31]  # 틱가치
            n = row[-31:-21].rstrip()  # 계약크기
            o = row[-21:-17].rstrip()  # 가격표시진법
            p = row[-17:-7]  # 환산승수
            q = row[-7:-6].rstrip()  # 최다월물여부 0:원월물 1:최다월물
            r = row[-6:-5].rstrip()  # 최근월물여부 0:원월물 1:최근월물
            s = row[-5:-4].rstrip()  # 스프레드여부
            t = row[-4:-3].rstrip()  # 스프레드기준종목 LEG1 여부 Y/N
            u = row[-3:].rstrip()  # 서브 거래소 코드

            df.loc[ridx] = [a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u]
            ridx += 1

    df.to_excel('해외 선물옵션 마스터.xlsx', index=False)  # 현재 위치에 엑셀파일로 저장
    os.remove('ffcode.mst.zip')
    os.remove('ffcode.mst')
    return df

#해외 
def get_overseas_index_master_dataframe():
    ssl._create_default_https_context = ssl._create_unverified_context
    urllib.request.urlretrieve("https://new.real.download.dws.co.kr/common/master/frgn_code.mst.zip", "frgn_code.mst.zip")
    frgn_code_zip = zipfile.ZipFile('frgn_code.mst.zip')
    frgn_code_zip.extractall()
    frgn_code_zip.close()
    file_name = "frgn_code.mst"
    # df1 : '구분코드','심볼','영문명','한글명'
    tmp_fil1 = "frgn_code_part1.tmp"
    tmp_fil2 = "frgn_code_part2.tmp"
    wf1 = open(tmp_fil1, mode="w")
    wf2 = open(tmp_fil2, mode="w")
    with open(file_name, mode="r", encoding="cp949") as f:
        for row in f:
            if row[0:1] == 'X':
                rf1 = row[0:len(row) - 14]
                rf_1 = rf1[0:1]
                rf1_2 = rf1[1:11]
                rf1_3 = rf1[11:40].replace(",", "")
                rf1_4 = rf1[40:80].replace(",", "").strip()
                wf1.write(rf1_1 + ',' + rf1_2 + ',' + rf1_3 + ',' + rf1_4 + '\n')
                rf2 = row[-15:]
                wf2.write(rf2 + '\n')
                continue
            rf1 = row[0:len(row) - 14]
            rf1_1 = rf1[0:1]
            rf1_2 = rf1[1:11]
            rf1_3 = rf1[11:50].replace(",", "")
            rf1_4 = row[50:75].replace(",", "").strip()
            wf1.write(rf1_1 + ',' + rf1_2 + ',' + rf1_3 + ',' + rf1_4 + '\n')
            rf2 = row[-15:]
            wf2.write(rf2 + '\n')
    wf1.close()
    wf2.close()
    part1_columns = ['구분코드', '심볼', '영문명', '한글명']
    df1 = pd.read_csv(tmp_fil1, header=None, names=part1_columns, encoding='utf-8')

    field_specs = [4, 1, 1, 1, 4, 3]
    part2_columns = ['종목업종코드', '다우30 편입종목여부', '나스닥100 편입종목여부',
                     'S&P 500 편입종목여부', '거래소코드', '국가구분코드']
    df2 = pd.read_fwf(tmp_fil2, widths=field_specs, names=part2_columns, encoding='utf-8')
    df2['종목업종코드'] = df2['종목업종코드'].str.replace(pat=r'[^A-Z]', repl=r'',
                                              regex=True)  # 종목업종코드는 잘못 기입되어 있을 수 있으니 참고할 때 반드시 mst 파일과 비교 참고
    df2['다우30 편입종목여부'] = df2['다우30 편입종목여부'].str.replace(pat=r'[^0-1]+', repl=r'', regex=True)  # 한글명 길이가 길어서 생긴 오타들 제거
    df2['나스닥100 편입종목여부'] = df2['나스닥100 편입종목여부'].str.replace(pat=r'[^0-1]+', repl=r'', regex=True)
    df2['S&P 500 편입종목여부'] = df2['S&P 500 편입종목여부'].str.replace(pat=r'[^0-1]+', repl=r'', regex=True)
    DF = pd.concat([df1, df2], axis=1)
    DF.to_excel('해외 주가지수 마스터.xlsx', index=False)  # 현재 위치에 엑셀파일로 저장
    os.remove(tmp_fil1)
    os.remove(tmp_fil2)
    os.remove('frgn_code.mst')
    os.remove('frgn_code.mst.zip')

    return DF

# 해외 주식 ETF 코드 마스터
def get_overseas_master_dataframe(val):
    ssl._create_default_https_context = ssl._create_unverified_context
    urllib.request.urlretrieve(f"https://new.real.download.dws.co.kr/common/master/{val}mst.cod.zip",   f"{val}mst.cod.zip")
    overseas_zip = zipfile.ZipFile(f'{val}mst.cod.zip')
    overseas_zip.extractall()
    overseas_zip.close()

# fo_stk_code_mts.mst.zip')

    columns = ['National code', 'Exchange id', 'Exchange code', 'Exchange name', 'Symbol', 'realtime symbol',
               'Korea name', 'English name', 'Security type(1:Index,2:Stock,3:ETP(ETF),4:Warrant)', 'currency',
               'float position', 'data type', 'base price', 'Bid order size', 'Ask order size',
               'market start time(HHMM)', 'market end time(HHMM)', 'DR 여부(Y/N)', 'DR 국가코드', '업종분류코드',
               '지수구성종목 존재 여부(0:구성종목없음,1:구성종목있음)', 'Tick size Type',
               '구분코드(001:ETF,002:ETN,003:ETC,004:Others,005:VIX Underlying ETF,006:VIX Underlying ETN)',
               'Tick size type 상세']



    df = pd.read_table( f"{val}mst.cod".upper(), sep='\t', encoding='cp949')
    df.columns = columns
    df.to_excel(f'{val} 종목코드 마스터.xlsx', index=False)  # 현재 위치에 엑셀파일로 저장

    return df

def get_overseas_master_all(val_list):
    # DF = pd.DataFrame()
    for i in val_list:
        print(i)
        temp = get_overseas_master_dataframe(i)
        # DF = pd.concat([DF,temp],axis=0)
        os.remove(f'{i}mst.cod'.upper())
        os.remove(f'{i}mst.cod.zip')




# get_overseas_index_master_dataframe()         # 해외 주가지수, 주식
# get_overseas_future_master_dataframe()        # 해외 선물옵션

get_domestic_future_master_dataframe()          # 국내 주가지수 선물옵션
get_domestic_com_future_master_dataframe()    # 국내 상품채권외환 선물옵션
get_domestic_stk_future_master_dataframe()    # 국내 주식 선물옵션

# get_kospi_master_dataframe()                  # 국내 코스피 주식 마스터
# get_kosdaq_master_dataframe()                 # 국내 코스닥 주식 마스터

# get_overseas_master_all(['nas','nys','ams','shs','shi','szs','szi','tse','hks','hnx','hsx'])




# lst = ['nas','nys','ams','shs','shi','szs','szi','tse','hks','hnx','hsx']
# DF=pd.DataFrame()
# for i in lst:
#     print(i)
#     temp = get_overseas_master_dataframe(i)
#     # DF = pd.concat([DF,temp],axis=0)
#     os.remove(f'{i}mst.cod'.upper())
#     os.remove(f'{i}mst.cod.zip')
# DF.to_excel('해외 종목 코드 마스터.xlsx',index=False)  # 전체 통합파일